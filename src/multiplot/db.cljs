(ns multiplot.db
  (:require [reagent.core :as r]
            [clojure.set :as cset]
            [instaparse.core :as insta :refer-macros [defparser]]
            [cljs-time.core :as time]
            [clojure.string :as s]
            [cljs.core.async :refer [put! chan <! >!]]
            [multiplot.views.graph :refer [selected-column]]
            [clojure.string :as str])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]))

;;Atom for holding the current parsed content that is being displayed
(def graph-series-data (r/atom (sorted-map)))

;;When a new file is read in, this atom is set to the name of that file
(def file-name (r/atom nil))

(def all-files (r/atom nil))

;;Tokenizer for parsing input in multiplot project
(defparser tokenizer
           "S= (row)*
           row = token (<tab> token)*(<newline>)*
           token= string | timestamp | number | decimal 
           timestamp = date <space> time
           date=month <delimiter> day <delimiter> year
           time=hour <delimiter> minute <delimiter> second
           <delimiter>=#'/' | #':'
           string = #'[^\\t\\n]+'
           decimal = #'[-]?[0-9]+\\.[0-9]+'\n
           number = #'[-]?[0-9]+'
           <space>=#'\\s+' | #'_'
           tab=#'\\t'
           month=#'[0-1]?[0-9]'
           day=#'[0-3]?[0-9]'
           year=#'[0-9]{4}'
           hour=#'[0-2]?[0-9]'
           minute=#'[0-6]?[0-9]'
           second=#'[0-6]?[0-9]'
           <newline>=#'\\n'")

(defn parse-input
  "Method for parsing input using tokenizer"
  [input-string]
  (js/console.log "Parsing input")
  (let [lines (s/split-lines input-string)]
    (map #(tokenizer %) lines)))

(defn get-in-multiple [input-vec & args]
  (map (partial get-in (vec input-vec)) (seq args)))

(defn parse-timestamp
  "Creates a cljs date object from the input vector produced by the tokenizer grammar"
  [& args]
  (apply time/date-time  (get-in-multiple args [0 3] [0 1] [0 2] [1 1] [1 2] [1 3])))

(defn parse-step 
  "Builds a string from the input vector and hopefully includes ending numbers and parentheses"
  [& args] 
  (apply str args))

(def transform-map
  (hash-map :S (fn [& args] (vec args))
            :row (fn [& args] (vec args))
            :token (fn [arg] arg)
            ; :string (fn [& args] (apply str args))
            :string parse-step
            :timestamp parse-timestamp
            :number js/parseInt
            :decimal js/parseFloat
            :month js/parseInt
            :day js/parseInt
            :year js/parseInt
            :hour js/parseInt
            :minute js/parseInt
            :second js/parseInt))


;;Extracts the result from the Javascript file reader
(def extract-result
  ; (map #(-> % .-target .-result js->clj))
  (map (fn [file-map] 
          (update file-map :parse-tree  #(-> % .-target .-result js->clj))))
  )

(def read-files (chan 1 extract-result))

(defn put-file
  [file]
  (let [reader (js/FileReader.)]
    (set! (.-onload reader) #(put! read-files {:name (.-name file) :parse-tree %}))
    (.readAsText reader file)))

(defn parse-file
  "Takes a file as an argument and parses it using the parser then updates the display state in parsed-output"
  [event]
  (let [target (.-currentTarget event)
        file (-> target .-files (aget 0))]
    (put-file file)))

(defn parse-files [event]
  (let [dt (.-dataTransfer event)
        allFiles (.-files dt)
        ;; TODO: try aclone and js->clj here
        files (vec (map aget (vec (repeat (.-length allFiles) allFiles)) (vec (range (.-length allFiles)))))]
        (reset! all-files files)
        (mapv put-file files))) 

(defn construct-series-data
  "Constructs the time series of data based on series name (at position 0 in the graph data)"
  [graph-data]
  (partition-by #(get % 0) graph-data))

(defn invert-vector-to-index-map
  "Reverses a vector to a map of vector value to index"
  [vec-to-invert]
  (cset/map-invert (into {} (map-indexed vector vec-to-invert))))


(defn extract-vector-from-value-seq
  "Returns a vector representation from the value sequence. Required to un-nest existing sequences"
  [val-seq]
  (vec (nth (nth val-seq 0) 0)))

(defn create-value-vector
  "Creates the a vector of vectors from the series-data vector. Ignores the timestamp and step columns from data."
  [series-data]
  (let [data nil]
    (map #(conj data (extract-vector-from-value-seq %)) series-data)))

(defn create-data-map
  "Constructs a map with headers and values for a data file"
  [series-data name]
  (let [headers (invert-vector-to-index-map (nth (nth (nth series-data 0) 0) 0))
        values (create-value-vector (drop 1 series-data))]
    {:name name :headers headers :values values}))

(def timestamp-offset (r/atom 0))

(defn hour-second-convert 
  [hour minute second]
  (+ (* hour 3600) (* minute 60) second))

(defn set-offset
  [offset]
  (reset! timestamp-offset offset)
  @timestamp-offset)

(defn convert-timestamp
  "Creates a cljs date object from the input string"
  [string-date]
  (let [split-strings (str/split string-date #" ")
        major-fields (str/split (nth split-strings 0) #"/")
        minor-fields (str/split (nth split-strings 1) #":")
        hour (js/parseInt (nth minor-fields 0))
        minute (js/parseInt (nth minor-fields 1))
        second (js/parseInt (nth minor-fields 2))]

        (- (hour-second-convert hour minute second) @timestamp-offset))) 

(defn offset-and-convert 
  [string-data]
  (let [numbered-line (concat (take 2 string-data) (map #(js/parseInt %) (drop 2 string-data)))
        vector-line (vec numbered-line)
        split-strings (str/split (second vector-line) #" ")
        minor-fields (str/split (nth split-strings 1) #":")
        hour (js/parseInt (nth minor-fields 0))
        minute (js/parseInt (nth minor-fields 1))
        second (js/parseInt (nth minor-fields 2))]
        (hour-second-convert hour minute second)))

(defn convert-numbers [line]
    (let [sample-label-row-index 4
          numbered-line (concat (take 2 line) (map #(js/parseInt %) (drop 2 line)))]
      (if (js/isNaN (nth numbered-line sample-label-row-index)) 
        [line] 
        [(update (vec numbered-line) 1 convert-timestamp)])))

(defn transform-parse-tree [{:keys [name parse-tree]}]
  "Takes in file data and passes off time series data to the create-data-map function"
  (let [lines (str/split-lines parse-tree)
        graph-input (mapv #(str/split % #"\t") lines)
        first-line (set-offset (offset-and-convert (second graph-input)))
        graph-data (mapv convert-numbers graph-input)
        series-data (construct-series-data graph-data)]
    (swap! graph-series-data assoc name (create-data-map series-data name))
    (reset! selected-column (-> @graph-series-data vals first :headers keys first))))

(go-loop []
         (let [input (<! read-files)]
          (transform-parse-tree input))
         (recur))