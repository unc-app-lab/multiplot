(ns multiplot.views.graph
  (:require [reagent.core :as r]
            [clojure.set :as cset]
            [cljsjs.plotly :as plotly]
            [clojure.string :as str]))

(def selected-column (r/atom "No data selected"))

(def plot-data-atom (r/atom []))

(defn update-column-selection
  "Updates the value stored in the column selection atom"
  [update]
  (reset! selected-column (.-value (.-target update))))

(defn get-columns-in-dataset
  "Returns a vector of columns in the dataset"
  [data]
  (if (nil? data)
    (vector "No data selected")
    (keys (get data :headers))))

(defn extract-timestamp-from-data
  "Creates a list of timestamps from input data"
  [headers data]
  (map #(nth (nth % 0) (get headers "Time stamp")) data))

(defn extract-id-num 
  "Gets id number from file name"
  [file]
  (nth (.-tail (str/split file " ")) 3))

(defn extract-column-from-data
  "Extracts selected column from data"
  [column-index data]
  (map #(nth (nth % 0) column-index) data))

(defn produce-graph-data
  "Produces a map of graph data where x is the sequence of timestamps and y is the values for the selected column"
  [graph-data selected-column]
  (if-not (empty? graph-data)
    (let [file-name (get graph-data :name)
          namer (extract-id-num file-name)
          headers (get graph-data :headers)
          data (get graph-data :values)]
      {:x (extract-timestamp-from-data headers data) :y (extract-column-from-data (get headers selected-column) data) :type "line" :name (extract-id-num file-name)})))


(defn in-vector
"helper function that checks if one element is in a vector"
 [vector elt]
  (some #(= elt %) vector))

(defn get-plotly-plot [plot-data-seq selected-column]
  (if-not (empty? plot-data-seq)
    (let [data (mapv #(produce-graph-data % selected-column) plot-data-seq)
          ytitle selected-column
          all-column-data (vec (map produce-graph-data
                                    plot-data-seq
                                    (vec (repeat (count plot-data-seq) selected-column))))]
      (if (.getElementById js/document "help-text") (.removeChild (.getElementById js/document "graph-container") (.getElementById js/document "help-text")) (js/console.log "nothing to remove"))
      (js/Plotly.newPlot
        "graph-container"
        (clj->js all-column-data)
        (clj->js {:margin {:t 8}
                  :yaxis {:title ytitle}
                  :xaxis {:title "Time in Milliseconds"}})))

     (if (= (.-length (.getElementsByClassName js/document "svg-container")) 0) 
     
      (let [div (.createElement js/document "DIV")
            node (.createElement js/document "P")
            textnode (.createTextNode js/document "Drag files into the gray box on the left or click on 'Upload from computer' to start graphing!")
            graph-container (.getElementById js/document "graph-container")]

        (.appendChild node textnode)
        (.appendChild div node)
        (.appendChild graph-container div)
        (.setAttribute node "id" "actual-help-text")
        (.add (.-classList div) "help-text")
        (.setAttribute div "id" "help-text")) 
        
        (js/console.log "no help text re-render"))
      ))

(defn deleteLine 
[deleting-lines]
(js/Plotly.deleteTraces "graph-container" deleting-lines))

(defn filter-column-values
  "Removes a sequence of header values from the header vector"
  [column-vector headers-to-remove]
  (vec (cset/difference (set column-vector) (set headers-to-remove))))

(defn generate-select-options-and-update-selected-column
  "Generates a return vector of select options and updates the selected column to be the first option"
  [graph-data-seq]
  (let [columns (filter-column-values
                  (get-columns-in-dataset (first graph-data-seq))
                  `("Step" "Time stamp"))]
    (map (fn [value] [:option {:key (str "opt" value) :value value} value]) columns)))

(defn graph-view [graph-data-seq selected-column]
  [:div {:class "graph-display-and-selection"}
   [:h1 "Graph Rendering"]
    [:div {:class "container-div"}
      [:div {:id "graph-container"}]
      [:div {:class "column-selection-form"}
      [:h2 "Select Column to Display"]
      [:select {:on-change update-column-selection, :value selected-column :class "column-selection"}
       (generate-select-options-and-update-selected-column graph-data-seq)]]]])

(defn graph-view-component [graph-data]
    (r/create-class
      {:display-name         "graph-component"
       :component-did-mount  (fn []
                               (get-plotly-plot (vals @graph-data) @selected-column))
       :component-did-update (fn []
                               (get-plotly-plot (vals @graph-data) @selected-column))
       :reagent-render       (fn []
                               (graph-view (vals @graph-data) @selected-column))}))
