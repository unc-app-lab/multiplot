(ns multiplot.views.main
  (:require [reagent.core :as r]
            [multiplot.db :as db]
            [multiplot.views.graph :as graph]
            [multiplot.views.files :as files]
            [clojure.string :as str]))

(def uploaded-files (r/atom []))

(defn in-vector
"helper function that checks if one element is in a vector"
 [vector elt]
  (some #(= elt %) vector))

(defn make-file-button [file-key]
  [:div {:id (str "uploaded-file-" file-key)}
    [:input {:type "checkbox" :id file-key}]
    [:label {:for "test"} file-key]])


(defn preventDefaults [e]
  (.preventDefault e)
  (.stopPropagation e))

(defn highlight [e]
  (.add (-> e .-target .-classList) "highlight"))

  (defn unhighlight [e]
  (.remove (-> e .-target .-classList) "highlight"))

(defn filenamer [file]
  (.-name file))

(defn drop-upload-files 
  "defines the drag and drop area for use in the reagent component below"
  []
  [:div {:id "drop-area"} 
        [:form {:class "my-form"}
          [:p "Drag to upload files"]
          [:input {:type "file" :id "fileElem" :accept ".log" :on-change db/parse-file}]
          [:label {:class "button" :for "fileElem"} "Upload from computer"]]])

(defn drag-and-drop 
  "reagent class that attaches function to the drag and drop area"
  []
      (r/create-class 
        {:display-name "dragAndDrop"
         :component-did-mount #(doto (.getElementById js/document "drop-area") 
                                    (.addEventListener "dragenter" preventDefaults)
                                    (.addEventListener "dragover" preventDefaults)
                                    (.addEventListener "dragleave" preventDefaults)
                                    (.addEventListener "drop" preventDefaults)
                                    (.addEventListener "dragenter" highlight)
                                    (.addEventListener "dragover" highlight)
                                    (.addEventListener "dragleave" unhighlight)
                                    (.addEventListener "drop" unhighlight)
                                    (.addEventListener "drop" db/parse-files))
         :reagent-render (fn []
                            (drop-upload-files))}))



(defn indexer
"helper function that takes in a term and a vector and returns where the term is in the vector"
[term search-vec]
  (.indexOf search-vec term))

(defn deselect 
  "function to deselect all checkbox items"
  []
  ; (js/console.log "deselect all")
  
  ; Cassette Test  20000720 (10-23-2017 13-32-48).log
  ; (set! (.-checked (.getElementById js/document (second (vec (keys @db/graph-series-data))))) false)
  
  (mapv #(set! (.-checked (.getElementById js/document %)) false) (vec (keys @db/graph-series-data))))

(defn remove-file
  "a function to remove selected files in file explorer. but first it'll print selected items"
  []
  (let [saved-files (vec (keys @db/graph-series-data))
        checked-files (vec (filter #(.-checked (.getElementById js/document %)) saved-files))
        trace-nums (vec (map #(.indexOf (vec (keys @db/graph-series-data)) %) checked-files))]
        (reset! db/graph-series-data (apply dissoc @db/graph-series-data checked-files))
        (graph/deleteLine (clj->js trace-nums))
        
        ; (map #(set! (.-checked (.getElementById js/document %)) false) (vec (keys @db/graph-series-data)))
        (deselect)
        )
        ; LAST THING IS TO RESET ALL THE CHECKS TO FALSE AFTER THIS
)

(defn clear-all 
  "clears all things on the graph"
  []
  (reset! db/graph-series-data (r/atom (sorted-map)))
  ; FUNCTION DOES NOT WORK
  )

(defn input-form-component
  "Returns a input form for uploading files and selecting columns from uploaded data"
  []
  [:div {:class "file-upload-and-explorer"}
    [:div {:class "explorer-main"}
    ; [:div {:class "file-ex-title"}
    ;   ]
    [:h3 {:id "uploaded-header"} "Uploaded files"]
    ; [files/file-explorer (map make-file-button @uploaded-files)]
    [files/file-explorer (map make-file-button (vec (keys @db/graph-series-data)))]
    [:div {:class "remove-div"} 
      [:button {:class "button" :onClick #(remove-file)} "Remove Files"]
      ; [:button {:class "button" :onClick #(js/alert "FILLER")} "Clear All"]
      ]]

   [:div {:class "explorer-sub"}
    ; [:h2 "Upload File Here"]
      [drag-and-drop]]])


(defn main-container []
  [:div {:style {:display "flex" :flex-direction "column"}}
    [:div {:class "header"}
      [:div {:class "header-wrap"} [:h2 "UNC Radiology Lab"]]]
    [:div {:class "main-content"}
      (input-form-component)
      [graph/graph-view-component db/graph-series-data]]])
