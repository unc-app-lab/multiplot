(ns multiplot.views.files
  )

; (defn remove-files 
;   "function to remove selected files in file explorer. but first it'll print selected items"
;   []
;   )

(defn file-explorer
  "A View for the file explorer component - accepts a parameter for the files to display"
  [files]
  [:div 
    [:form {:class "file-explorer"}
    [:ul
      (map (fn [file] [:li {:key file} file]) files)]]
    ; [:button {:class "remove-button" :onClick #(js/alert "remove button")} "Remove Selected Files"]
    ])

