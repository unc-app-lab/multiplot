(ns multiplot.core
    (:require [reagent.core :as r]
              [multiplot.views.main :refer [main-container]]))

;; define your app data so that it doesn't get over-written on reload

(defonce app-state (atom {:text "Hello world!"}))


(defn start []
  (r/render-component
    [main-container]
    (js/document.getElementById "app")))

(start)

(defn on-js-reload []
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
)


