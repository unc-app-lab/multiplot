# Multiplot Project
## Project Summary
The goal of this project is to create a web application for plotting multiple data series to identify and report anomalies.
The web app is built using CLJS, Reagent (A react wrapper for CLJS), and PlotlyJs. 

## Project Goal
The file "multiplot_option.pdf" has a mock-up of what the expected UI will look like. A User should be able to add data
files, select columns to display, and save displayed graph.

## Project Structure 
The project is separated into two main components. The DB class contains the logic for reading, parsing, and processing
the data files. The views package has classes for the file  view, graph view, and overall view. The file view is the
interface through which the user interacts with uploaded files or uploads new files. The graph view displays the Poltly
graph as well as any interfaces that interact with it, such as column selection. The overall view renders both the 
graph and file view in a flex box layout the gives the file view 1/3 of the screen, and the graph view the remainder.

## Running The Project
The project uses leiningen and Figwheel for development.

- Download Java 8 SDK from https://www.oracle.com/technetwork/java/javase/downloads/java-archive-javase8u211-later-5573849.html
    - Note: newer versions of java may or may not work

On Windows:
- Download leiningen from https://djpowell.github.io/leiningen-win-installer/

On Mac and Linux:
- Download lein script from https://leiningen.org/

 In order to run the development server, run `lein figwheel`
in the multiplot directory. This should open a local development server available at localhost:3449.
